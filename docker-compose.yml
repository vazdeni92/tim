version: "3.7"
x-caddy:
 &caddy-common
 image: caddy:2.3.0
 command: caddy run --config /etc/caddy/Caddyfile
 read_only: true
 volumes:
  - caddy_data:/data
  - caddy_config:/config
  - ./certs/caddy:/certs:ro
  - ./Caddyfile:/etc/caddy/Caddyfile:ro
  - ./timApp/static:/tim/timApp/static:ro
  - ${LOG_DIR:?}/caddy:/logs
  - ./timApp/modules/cs/generated:/tim/timApp/modules/cs/generated:ro
  - ./timApp/modules/cs/static:/tim/timApp/modules/cs/static:ro
  - csplugin_data:/cs_data:ro
  - csplugin_data_generated:/csgenerated:ro
 depends_on:
  - jsrunner
 environment:
  CADDY_DOMAINS: ${CADDY_DOMAINS:?}
  CADDY_EXTRA_CONFIG: ${CADDY_EXTRA_CONFIG:?}
  CADDY_EXTRA_TIM_CONFIG: ${CADDY_EXTRA_TIM_CONFIG:-}
 restart: unless-stopped

services:
 tim:
  image: timimages/tim:${TIM_IMAGE_TAG:?}
  build: ./timApp
  depends_on:
   # - stack-api-server
   - postgresql
   - csplugin
   - pali
   - fields
   - showfile
   - haskellplugins
   - imagex
   - dumbo
   - feedback
   - redis
   - oiko
  volumes:
   - .:/service
   - ${LOG_DIR:?}:/service/tim_logs
   - ${FILES_ROOT:?}:/tim_files
   - cache:/cache
  working_dir: /service/timApp
  command: python3 launch.py --with-gunicorn
  environment:
   FLASK_APP: tim.py
   PYTHONPATH: /service
   COMPOSE_PROJECT_NAME: ${COMPOSE_PROJECT_NAME:?}
   TIM_SETTINGS: ${CONFIG_FILE}
   TIM_HOST: ${TIM_HOST:?}
   PG_MAX_CONNECTIONS: ${PG_MAX_CONNECTIONS:?}
   COMPOSE_PROFILES: ${COMPOSE_PROFILES:?}
  restart: unless-stopped
  networks:
   - db
   - default
  read_only: true
  tmpfs:
   - /root
   - /tmp
   - /var/cache
   - /var/log
   - /run/gunicorn
  profiles:
   - prod
   - prod_multi
   - dev
 timagent:
  profiles:
   - prod
   - prod_multi
   - dev
  image: timimages/timagent
  build:
   context: ./timApp
   dockerfile: DockerfileAgent
   args:
    TIM_IMAGE_TAG: ${TIM_IMAGE_TAG:?}
  command: sleep infinity
  restart: unless-stopped
  read_only: true
 postgresql:
  profiles:
   - prod
   - prod_multi
   - dev
  image: postgres:11
  volumes:
   - data11:/var/lib/postgresql/data
  command: [
    "postgres",
    "-c", "log_statement=mod",
    "-c", "max_connections=${PG_MAX_CONNECTIONS:?}",
  ]
  restart: unless-stopped
  networks:
   - db
  read_only: true
  shm_size: ${PG_SHM_SIZE:?}
  tmpfs:
   - /run/postgresql
   - /tmp
  environment:
   POSTGRES_PASSWORD: postgresql
  logging:
   # According to docs, "local" is more compact and performant than the default json-file driver.
   driver: local
   options:
    max-size: "50m"
    max-file: "100"
 postgresql-test:
  profiles:
   - test
   - dev
  image: postgres:11
  tmpfs:
   - /var/lib/postgresql/data
  environment:
   POSTGRES_PASSWORD: postgresql
 csplugin:
  image: timimages/cs3${CSPLUGIN_TARGET:-}:elixir
  build:
   context: ./timApp/modules/cs
   target: ${CSPLUGIN_TARGET:-complete}
  volumes:
   - ${FILES_ROOT:?}/blocks/uploads:/uploads:ro
   - ./timApp/modules/cs:/cs:ro
   - /var/run/docker.sock:/var/run/docker.sock
   - ./timApp/modules/cs/static:/csstatic:ro
   - ./tim_common:/cs/tim_common:ro
   - /tmp/${COMPOSE_PROJECT_NAME:?}_uhome:/tmp
   - ${LOG_DIR:?}/cs:/logs
   - /root/.ssh/
   - csplugin_data:/cs_data
   - csplugin_data_generated:/csgenerated
  networks:
   - csplugin_db
   - default
  working_dir: /cs
  command: ./startPlugins.sh
  environment:
   PYTHONUNBUFFERED: "1"
   TIM_ROOT: ${TIM_ROOT:?}
   TIM_HOST: ${TIM_HOST:?}
   COMPOSE_PROJECT_NAME: ${COMPOSE_PROJECT_NAME:?}
   CSPLUGIN_TARGET: ${CSPLUGIN_TARGET:-}
  user: root  # We need access to Docker socket.
  restart: unless-stopped
  read_only: true
 csplugin_mongo: # MongoDB for csplugin
  image: mongo:5
  profiles:
   - prod
   - prod_multi
   - dev
  restart: unless-stopped
  environment:
   MONGO_INITDB_ROOT_USERNAME: mongo
   MONGO_INITDB_ROOT_PASSWORD: mongodb
  volumes:
   - csplugin_db_data:/data/db
  networks:
   - csplugin_db
 csplugin_cassandra: # Cassandra for csplugin
  profiles:
   - prod
   - prod_multi
   - dev
  image: cassandra:4
  restart: unless-stopped
  cap_add: # Needed for Cassandra to be able to bind to the Docker socket.
   - SYS_NICE
  environment:
   # We're not going to enable replication, so it's fine to listen to localhost
   CASSANDRA_LISTEN_ADDRESS: localhost
  volumes:
   - csplugin_db_data:/var/lib/cassandra
   - ./timApp/modules/cs/cassandra/cassandra.yaml:/etc/cassandra/cassandra.yaml
  networks:
   - csplugin_db
 haskellplugins:
  image: timimages/haskellplugins:latest
  build:
   context: ./timApp/modules/Haskell
   dockerfile: Dockerfile
  volumes:
   - ./timApp/modules/Haskell:/Haskell
  working_dir: /Haskell
  command: ./startAll.sh
  restart: unless-stopped
  read_only: true
 showfile:
  image: timimages/cs3${CSPLUGIN_TARGET:-}:elixir
  volumes:
   - ./timApp/modules/svn:/app/svn:ro
   - ./tim_common:/app/tim_common:ro
  working_dir: /app/svn
  environment:
   PYTHONPATH: /app
  command: ./startAll.sh
  restart: unless-stopped
  read_only: true
  tmpfs:
    - /tmp
 imagex:
  image: timimages/cs3${CSPLUGIN_TARGET:-}:elixir
  volumes:
   - ./timApp/modules/imagex:/app/imagex:ro
   - ./tim_common:/app/tim_common:ro
  working_dir: /app/imagex
  environment:
   PYTHONPATH: /app
  command: ./startAll.sh
  restart: unless-stopped
  read_only: true
 pali:
  image: timimages/tim:${TIM_IMAGE_TAG:?}
  build: ./timApp/modules/pali
  volumes:
   - ./timApp/modules/pali:/app/pali:ro
   - ./tim_common:/app/tim_common:ro
  working_dir: /app/pali
  environment:
    PYTHONPATH: /app
    COMPOSE_PROFILES: ${COMPOSE_PROFILES:?}
  command: ./startAll.sh
  restart: unless-stopped
  read_only: true
  tmpfs:
   - /tmp
 fields:
  image: timimages/tim:${TIM_IMAGE_TAG:?}
  build: ./timApp/modules/fields
  volumes:
   - ./timApp/modules/fields:/app/fields:ro
   - ./tim_common:/app/tim_common:ro
  working_dir: /app/fields
  environment:
   PYTHONPATH: /app
  command: ./startAll.sh
  restart: unless-stopped
  read_only: true
  tmpfs:
   - /tmp
 jsrunner:
  image: timimages/tim:${TIM_IMAGE_TAG:?}
  #build: ./timApp/modules/multisave
  volumes:
   - ./timApp/modules/jsrunner:/timApp/modules/jsrunner:rw
   - ./timApp/static/scripts:/timApp/static/scripts:ro
  working_dir: /timApp/modules/jsrunner/server
  command: ./startAll.sh
  restart: unless-stopped
  read_only: true
  tmpfs:
   - /root/.npm
   - /root/.config
 drag:
  image: timimages/tim:${TIM_IMAGE_TAG:?}
  volumes:
   - ./timApp/modules/drag:/app/drag:ro
   - ./tim_common:/app/tim_common:ro
  working_dir: /app/drag
  environment:
   PYTHONPATH: /app
  command: ./startAll.sh
  restart: unless-stopped
  read_only: true
  tmpfs:
   - /tmp
 feedback:
  image: timimages/tim:${TIM_IMAGE_TAG:?}
  volumes:
   - ./timApp/modules/feedback:/app/feedback:ro
   - ./tim_common:/app/tim_common:ro
  working_dir: /app/feedback
  environment:
   PYTHONPATH: /app
  command: ./startAll.sh
  restart: unless-stopped
  read_only: true
  tmpfs:
   - /tmp
 dumbo:
  image: timimages/dumbo:${TIM_IMAGE_TAG:?}
  build:
   context: ./Dumbo
   dockerfile: Dockerfile
   args:
    TIM_IMAGE_TAG: ${TIM_IMAGE_TAG:?}
  command: ./Dumbo --port 5000 --cacheDir /dumbocache --tmpDir /dumbotmp
  working_dir: /Dumbo
  volumes:
   - ./Dumbo/cache:/dumbocache
   - ./Dumbo/tmp:/dumbotmp
  logging:
   driver: none
  restart: unless-stopped
  read_only: true
 redis:
  image: redis
  command: ["redis-server", "--appendonly", "yes"]
  restart: unless-stopped
  read_only: true
  volumes:
  - redis_data:/data
 celery:
  image: timimages/tim:${TIM_IMAGE_TAG:?}
  command: celery worker -A timApp.tim_celery.celery --concurrency 4
  volumes:
   - .:/service
   - ${LOG_DIR:?}:/service/tim_logs
   - ${FILES_ROOT:?}:/tim_files
   - cache:/cache
  working_dir: /service/timApp
  depends_on:
   - postgresql
   - redis
  environment:
   FLASK_APP: tim.py
   PYTHONPATH: /service
   COMPOSE_PROJECT_NAME: ${COMPOSE_PROJECT_NAME:?}
   TIM_SETTINGS: ${CONFIG_FILE}
   TIM_HOST: ${TIM_HOST:?}
  restart: unless-stopped
  networks:
   - db
   - default
  read_only: true
  tmpfs:
   - /tmp
 celery-beat:
  image: timimages/tim:${TIM_IMAGE_TAG:?}
  command: >-
   celery beat
   -A timApp.tim_celery.celery
   -S timApp.celery_sqlalchemy_scheduler.schedulers:DatabaseScheduler
   --pidfile /var/run/celery/celerybeat.pid
  volumes:
   - .:/service
   - ${LOG_DIR:?}:/service/tim_logs
   - ${FILES_ROOT:?}:/tim_files
   - cache:/cache
  working_dir: /service/timApp
  depends_on:
   - postgresql
   - redis
  environment:
   FLASK_APP: tim.py
   PYTHONPATH: /service:/service/timApp/modules/py
   COMPOSE_PROJECT_NAME: ${COMPOSE_PROJECT_NAME:?}
   TIM_SETTINGS: ${CONFIG_FILE}
   TIM_HOST: ${TIM_HOST:?}
  restart: unless-stopped
  networks:
   - db
   - default
  read_only: true
  tmpfs:
   - /tmp
   - /run/celery
 maxima:
  profiles:
   - prod
   - prod_multi
   - dev
  image: timimages/goemaxima:2020113001-latest
  restart: unless-stopped
 stack-api-server:
  profiles:
   - prod
   - prod_multi
   - dev
  image: timimages/stack-api:latest
  depends_on:
   - maxima
  restart: unless-stopped
  environment:
   BROWSER_URL: ${TIM_HOST:?}
 oiko:
  image: timimages/tim:${TIM_IMAGE_TAG:?}
  command: gunicorn -b 0.0.0.0:5000 app:app
  restart: unless-stopped
  working_dir: /service
  read_only: true
  volumes:
   - ./oiko:/service:ro
  tmpfs:
   - /tmp
 caddy:
  <<: *caddy-common
  profiles:
   - prod
   - dev
   - test
  ports:
   - "80:80"
   - "443:443"
 caddy_multi:
  <<: *caddy-common
  profiles:
   - prod_multi
  ports:
  - "${CADDY_MULTI_PORT}:80"
 mailman-test:
  image: timimages/mailman-core:dev
  profiles:
   - test
   - dev
  volumes:
   - ./mailman/core/mailman-extra.cfg:/opt/mailman/mailman-extra.cfg
   - ./mailman/core/mailman-rest-event.cfg:/opt/mailman/mailman-rest-event.cfg
  environment:
   DATABASE_TYPE: sqlite
   DATABASE_URL: sqlite:////opt/mailmandb/mailmanweb.db
   DATABASE_CLASS: mailman.database.sqlite.SQLiteDatabase
   INIT_DEV: 1
  tmpfs:
   - /opt/mailmandb
 tests:
  profiles:
   - test
  image: timimages/tim:${TIM_IMAGE_TAG:?}
  command: "python3 -m unittest ${TEST_PARAMS:-discover tests/ 'test_*.py' .}"
  volumes:
   - .:/service:rw
  environment:
   TIM_SETTINGS: testconfig.py
   PYTHONPATH: /service
   GITLAB_CI: ${GITLAB_CI:-false}
  depends_on:
   - csplugin
   - pali
   - fields
   - showfile
   - haskellplugins
   - imagex
   - dumbo
   - feedback
   - redis
   - oiko
   - caddy
   - postgresql-test
   - mailman-test
  tmpfs:
   - /tmp
   - /tim_files
   - /cache
  working_dir: /service/timApp
  read_only: true
volumes:
 data11:
 cache:
 caddy_data:
 caddy_config:
 redis_data:
 csplugin_db_data:
 csplugin_data:
 csplugin_data_generated:
networks:
 db:
 csplugin_db:
